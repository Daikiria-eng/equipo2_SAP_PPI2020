import Logo from './assets/RenderLogo.png';
import LogoWhite from './assets/RenderLogoWhite.png';

export default [
    {
        "img": LogoWhite
    },{
        "img": Logo
    },
]